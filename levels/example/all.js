[
    { "command": "log", "message":"Connected" },
    { "command": "init" },

    { "command": "add", "id":"scene", "type":"scene" },
    { "command": "add", "id": "camera", "type":"camera", "position": [0,-80,110], "rotation":[45,0,0] },

    { "command": "add", "id": "ambientlight", "type":"pointlight", "shadow": false, "position": [0, 50, 500], "color": "#ffffff", "intensity": 2.0, "distance": 601 },

    { "command": "add", "id": "ambientlight", "type":"pointlight", "shadow": true, "position": [-75, -25, 100], "color": "#ffffff", "intensity": 1.0, "distance": 200 },

    { "command": "add", "id":"floor","type":"plane", "position": [ 0,0,0],  "rotation":[0,0,0], "width":200, "height":200, "color": "#b0b0b0" },
    { "command": "add", "id":"floorgrid","type":"grid", "position": [ 0,0,0], "rotation":[90,0,0], "divisions": 20,  "size":200, "color": "#000000" },

    { "command": "loadjs", "file":"js/characters/square.js"}

]
