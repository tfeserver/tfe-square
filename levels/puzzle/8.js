// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": false,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"3",
        "camera_position":[30,-10,100],
        "camera_look":[30,-10,0],
        "best_moves": 1,
        "map":
        {
            "width": 9,
            "height": 9,
            "invalid_positions":
            [
                { "x": 2, "y": 2},
                { "x": 6, "y": 2},
                { "x": 6, "y": 6},
                { "x": 2, "y": 6},
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 3, "y": 3 }, // First item, bottom left
                { "x": 4, "y": 3 }, // First item, bottom left
                { "x": 5, "y": 3 }, // First item, bottom left
                { "x": 3, "y": 4 }, // First item, bottom left
                { "x": 4, "y": 4 }, // First item, bottom left
                { "x": 5, "y": 4 }, // First item, bottom left
                { "x": 3, "y": 5 }, // First item, bottom left
                { "x": 4, "y": 5 }, // First item, bottom left
                { "x": 5, "y": 5 }, // First item, bottom left
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 99, "y": 0  }
        }
    });
})(this);


