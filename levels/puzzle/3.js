// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"1",
        "best_moves": 4,
        "map":
        {
            "width": 5,
            "height": 5,
            "invalid_positions":
            [
                { "x": 2, "y": 2}
            ],
            // where the items are initially
            "items_data":
            [
                { "x": 1, "y": 1 }, // First item, bottom left
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 3, "y": 3, "rotation":"180"  }
        }
    });
})(this);


