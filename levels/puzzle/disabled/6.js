// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"3",
        "best_moves": 24,
        "map":
        {
            "width": 7,
            "height": 7,
            // where the items are initially
            "items_data":
            [
                { "x": 1, "y": 1 },
                { "x": 3, "y": 2, "playable": false },
                { "x": 5, "y": 1 },
                { "x": 2, "y": 3, "playable": false },
                { "x": 3, "y": 3, "playable": false },
                { "x": 4, "y": 3, "playable": false },
                { "x": 1, "y": 5 },
                { "x": 3, "y": 4, "playable": false },
                { "x": 5, "y": 5 },
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 2, "y": 2, "rotation":0  }
        }
    });
})(this);


