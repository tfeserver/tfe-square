// we are in ux environnement here
(function(ux)
{
    let main_bg_color = '#071738';
    let grid_color = '#071738';
    let floor_color = '#bbbbbb';
    let faces_color = [ '#00bb00', '#bb00bb', '#bbbb00', '#00bbbb', '#bbbb00', '#0000bb' ];
    let default_color = '#3b5893';

    let default_params = 
    {
        x :0,
        y:0,
        materials:
        {
            'x' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[0] },
            '-x' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[1] },
            'y' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[2] },
            '-y' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[3] },
            'z' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[4] },
            '-z' : { metalness: 0.5, opacity:1, roughness: 0.3, color: faces_color[5] },
        }
    };
    let used_pos = {
    };

    let instances = [];
    let selected_instance = null;
    let selected_instance_idx = -1;
    let action_timer = null;
    let menu_timer = null;

    function posToString (obj)
    {
        return obj.x+'_'+obj.y;
    }

    function doAction()
    {
        this.action_timer=null;
    }

    function manualSelection(instance)
    {
        if(this.action_timer) { return false; }
        this.action_timer = window.setTimeout(doAction.bind(this), 500);
        selected_instance.disable_selected();
        selected_instance = instance;
        selected_instance.enable_selected();
        return false;
    }

    function nextSelection(with_timer)
    {
        if(this.action_timer) { return false; }
        if(with_timer)
        {
            this.action_timer = window.setTimeout(doAction.bind(this), 500);
        }
        selected_instance.disable_selected();

        selected_instance_idx++;
        selected_instance_idx = selected_instance_idx >= instances.length ? 0 : selected_instance_idx;
        selected_instance = instances[selected_instance_idx];
        selected_instance.enable_selected();
        return false;
    }
    function prevSelection(with_timer)
    {
        if(this.action_timer) { return false; }
        if(with_timer)
        {
            this.action_timer = window.setTimeout(doAction.bind(this), 500);
        }
        selected_instance.disable_selected();

        selected_instance_idx--;
        selected_instance_idx = selected_instance_idx < 0 ?  instances.length -1 : selected_instance_idx;
        selected_instance = instances[selected_instance_idx];
        selected_instance.enable_selected();
        return false;
    }

    function prevSelection(with_timer)
    {
        if(this.action_timer) { return false; }
        if(with_timer)
        {
            this.action_timer = window.setTimeout(doAction.bind(this), 500);
        }
        selected_instance.disable_selected();

        selected_instance_idx--;
        selected_instance_idx = selected_instance_idx <0 ? instances.length-1 : selected_instance_idx;
        selected_instance = instances[selected_instance_idx];
        selected_instance.enable_selected();
        return false;
    }

    function moveSquare(direction, from_ia)
    {
        if(!this.playable && !from_ia) {  return false; }
        let destination = {x: selected_instance.x, y: selected_instance.y};
        switch(direction)
        {
            case 'left': destination.x--; break;
            case 'right': destination.x++; break;
            case 'top': destination.y++; break;
            case 'bottom': destination.y--; break;
        }
        if(!used_pos[posToString(destination)] && 
            destination.x>=0 && 
            destination.y>=0 && 
            destination.x< this.logic_params.map.width &&
            destination.y< this.logic_params.map.height
        )
        {
            delete used_pos[posToString(selected_instance)];
            if(selected_instance.trigger_move(direction, checkAfterMove.bind(this)))
            {
                if(typeof this.num_moves!='undefined')
                {
                    this.num_moves++;
                }
                else 
                {
                    this.num_moves=1;
                }
                this.updateMovesValue();
                used_pos[posToString(destination)]=1;
            }
        }
    }

    ux.updateMovesValue = function()
    {
        document.querySelector('.moves-value').innerHTML=this.num_moves+' / '+this.best_moves;
    }

    function checkAfterMove()
    {
        selected_instance.correct_position =
            selected_instance.x==selected_instance.correct_x &&
            selected_instance.y == selected_instance.correct_y &&
            selected_instance.coords.x.coord == selected_instance.correct_coords.x[0] &&
            selected_instance.coords.x.delta == selected_instance.correct_coords.x[1] &&
            selected_instance.coords.y.coord == selected_instance.correct_coords.y[0] &&
            selected_instance.coords.y.delta == selected_instance.correct_coords.y[1] &&
            selected_instance.coords.z.coord == selected_instance.correct_coords.z[0] &&
            selected_instance.coords.z.delta == selected_instance.correct_coords.z[1];

        if(selected_instance.correct_position)
        {
            let win = true;
            instances.forEach((instance) => 
            {
                win = win && instance.correct_position;
            });
            if(win)
            {
                this.win();
            }
        }
    }

    ux.start_timer = function()
    {
        this.internal_timer=new Date();
        // Time start now
        window.clearInterval(this.time_timer);
        this.time_timer =window.setInterval(this.time_move.bind(this), 500);
    }

    ux.win = function()
    {
        this.playable=false;
        window.clearInterval(this.time_timer);

        let start_time  = new Date().getTime();
        document.querySelector('.completed').classList.add('visible');
        document.querySelector('.ui-playing').classList.remove('visible');
        this.animate('camera_move', (delta) =>
        {
            console.log('delta',delta);
            let diff = (delta* 50000) / 2000;
            this.ids.camera.rotation.x+=degToRad(diff);
            return true;
        });
        window.setTimeout(() => 
        {
            this.num_moves=0;
            this.best_moves=0;
            this.updateMovesValue();
            document.querySelector('.completed').classList.remove('visible');

            this.client.send({ command:'start_level', level: ++this.current_level});
            this.start_timer();
        }, 2000);
    };

    ux.time_move = function()
    {
        var diff = parseInt(((new Date()) - this.internal_timer)/1000, 10);
        var min = Math.floor(diff / 60);
        var secs =  diff - (min*60);
        document.querySelector('.time-value').innerText= (min<10 ? '0'+min : min)+':'+(secs<10 ? '0'+secs : secs);

    },

    ux.reset_game = function()
    {
        used_pos = { };
        instances = [];
        selected_instance = null;
        selected_instance_idx = -1;
        action_timer = null;
        this.reset_clickables(instances);
    };

    ux.start_level = function(level)
    {
        this.num_moves=0;
        this.best_moves;
        this.start_timer();
        document.querySelector('.moves-value').innerHTML=this.num_moves;
        this.updateMovesValue();
        document.querySelector('.main').classList.remove('visible');
        document.querySelector('.bottombar').classList.add('visible');

        this.current_level = level;
        this.send({ data: {command: 'start_level', level: level}});
    };
    ux.toggle_menu = function()
    {
        if(this.action_timer) { return false; }
        console.log('toggle_menu ');
        this.action_timer = window.setTimeout(doAction.bind(this), 500);
        if(document.querySelector('.main').classList.contains('visible'))
        {
            document.querySelector('.ui-playing').classList.add('visible');
            document.querySelector('.main').classList.remove('visible');
            document.querySelector('.main_content').classList.remove('paused');
            console.log('jere',this.logic_params);
            this.playable=this.logic_params.playable;
        }
        else
        {
            document.querySelector('.ui-playing').classList.remove('visible');
            document.querySelector('.main').classList.add('visible');
            document.querySelector('.main_content').classList.add('paused');
            this.playable=false;
        }
    };

    ux.start_game = function()
    {
        // UI
        this.add({ "id":"ui","type":"html", "class": "ui","html": `
            <style>
                * { margin:0; padding:0; box-sizing:border-box; }
                .ui
                {
                    font-family:sans-serif;
                    position:absolute;
                    overflow:hidden;
                    top:0;
                    bottom:0;
                    left:0;
                    right:0;
                    --size:8vh;
                }
                .main
                {
                    position:absolute;
                    width:100%;
                    bottom:0;
                    right:0;
                    top:0;
                    text-align:center;
                    color:white;
                    text-transform:uppercase;
                    opacity:0;
                    pointer-events:none;
                    transition:opacity 0.5s ease 0s;
                }
                .completed
                {
                    pointer-events:none;
                    position:absolute;
                    top:50%;
                    left:50%;
                    transform:translateX(-50%) translateY(-40%);
                    padding:0 20px;
                    opacity:0;
                    transition:none;
                    color:white;
                    text-align:center;
                    font-size:10vh;
                    font-weight:bold;
                }
                .main_content
                {
                    position:absolute;
                    right:0;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    height:100%;
                    justify-content:center;
                }
                .main_content.paused
                {
                    background:rgba(0,0,0,0.8);
                }
                .main.visible
                {
                    pointer-events:all;
                    opacity:1;
                    z-index:100;
                }
                .completed.visible
                {
                    opacity:1;
                    transform:translateX(-50%) translateY(-50%);
                    transition:opacity 0.5s ease 1.0s, transform 0.5s ease 1.0s;
                }
                .main h1
                {
                    padding-bottom:calc(var(--size) * 0.5);
                    font-size:calc(var(--size) * 0.5);
                    margin-left:50%;
                    width:50%;
                }
                .main h1 span
                {
                    display:inline-block;
                    padding-bottom:5px;
                    border-bottom:1px solid white;
                }
                .bottombar
                {
                    position:absolute;
                    bottom:0;
                    width:100%;
                    height:11vh;
                    line-height:3vh;
                    background:rgba(0,0,0,0.8);
                    text-transform:uppercase;
                    color:white;
                    padding:0 11vh;
                    display:flex;
                    flex-direction:row;
                    align-items:center;
                    opacity:0;
                }
                .visible.bottombar
                {
                    opacity:1;
                }
                .bottombar-square
                {
                    width:25%;
                }
                .bottombar-square button
                {
                    background:transparent;
                    font-size:7vh;
                    border:none;
                    cursor:pointer;
                }
                .bottombar-square.centered
                {
                    text-align:center;
                }
                .bottombar-square.right
                {
                    text-align:right;
                }
                .bottombar-label
                {
                    display:block;
                    font-size:2vh;
                }
                .bottombar-value
                {
                    font-size:4vh;
                }
                .menu 
                {
                    margin-left:50%;
                    width:50%;
                    letter-spacing:0.05em;
                }
                .menu ul
                {
                    list-style:none;
                }
                .menu li
                {
                    line-height:var(--size);
                }
                .menu li.selected button
                {
                    text-shadow:0px 0px 3px white;
                    animation: 0.5s infinite alternate shine;
                }
                @keyframes shine
                {
                    from { text-shadow:0px 0px 5px white; }
                    to { text-shadow:0px 0px 10px white; }
                }
                .menu button
                {
                    font-size:calc( var(--size) * 0.4);
                    background:none;
                    border:none;
                    text-align:center;
                    color:white;
                }
                .menu button:hover,
                .menu button:focus
                {
                    color:#aaa;
                }
                .menu button:active
                {
                    color:#555;
                }
                .reset_line
                {
                    display:none;
                }

.ui-playing
{
    opacity:0;
    pointer-events:none;
    transition:opacity 0.3s ease 0s;
}
.ui-playing.visible
{
    opacity:1;
    pointer-events:all;
    transition:opacity 1s ease 0s;
}
.ui-arrows
{
    position:absolute;
    top:0;
    left:0;
    right:0;
    bottom:11vh;
    opacity:0.1;
}
.ui-arrows div
{
    position:absolute;
    width:35%;
    height:40%;
    background:transparent;
    border:none;
    text-align:center;
}
.ui-arrows div:before
{
    position:absolute;
    width:25vh;
    height:25vh;
    background:white;
    border-radius:100%;
    top:50%;
    left:50%;
    transform:translateX(-50%) translateY(-50%);
    font-size:20vh;
    color:black;
}
.arrow-top { top: 0; left: 50%; transform:translateX(-50%); z-index:2; }
.arrow-top:before { content:'↑'; }
.arrow-right { top: 50%; right: 0%; transform:translateY(-50%); }
.arrow-right:before { content:'→'; }
.arrow-bottom { bottom: 0%; left: 50%; transform:translateX(-50%); z-index:2; }
.arrow-bottom:before { content:'↓'; }
.arrow-left { left: 0%; top: 50%; transform:translateY(-50%); }
.arrow-left:before { content:'←'; }

            </style>
            <div class="ui-playing">
                <div class="ui-arrows">
                    <div class="arrow-top"></div>
                    <div class="arrow-right"></div>
                    <div class="arrow-bottom"></div>
                    <div class="arrow-left"></div>
                </div>
                <div class="bottombar">
                    <div class="bottombar-square bottombar-menu">
                        <button class="bottombar-home">🏠</button>
                    </div>
                    <div class="bottombar-square left">
                        <label class="bottombar-label">Level:</label>
                        <span class="bottombar-value level-value"></span>
                    </div>
                    <div class="bottombar-square centered">
                        <label class="bottombar-label">Nº Moves:</label>
                        <span class="bottombar-value moves-value">0 / 0</span>
                    </div>
                    <div class="bottombar-square right">
                        <label class="bottombar-label">Time:</label>
                        <span class="bottombar-value time-value">00:00</span>
                    </div>
                </div>
            </div>
            <div class="completed">Level completed!</div>
            <div class="main visible">
                <div class="main_content">
                    <h1><span>Tfe Square</span></h1>
                    <div class="menu">
                        <ul>
                            <li class="continue_line selected"><button class="continue_game">Continue</button></li>
                            <li class="new_game_line selected"><button class="new_game">New game</button></li>
                            <li class="restart_line selected"><button class="restart_level">Restart level</button></li>
                            <li class="reset_line"><button class="reset">Reset</button></li>
                        </ul>
                    </div>
                </div>
            </div>
        `});

        // Start background
        this.client.send({ command:'start_level', level: 0});

        document.querySelector('.new_game').addEventListener('click', this.start_level.bind(this,this.current_level));
        document.querySelector('.bottombar-home').addEventListener('click', this.toggle_menu.bind(this));
        document.querySelector('.continue_game').addEventListener('click', this.toggle_menu.bind(this));

        document.querySelector('.arrow-left').addEventListener('click', moveSquare.bind(this,'left'), false);
        document.querySelector('.arrow-top').addEventListener('click', moveSquare.bind(this,'top'), false);
        document.querySelector('.arrow-bottom').addEventListener('click', moveSquare.bind(this,'bottom'), false);
        document.querySelector('.arrow-right').addEventListener('click', moveSquare.bind(this,'right'), false);
    };

    ux.random_moves = function()
    {
        selected_instance_idx = Math.round(Math.random()* (instances.length-1));
        selected_instance = instances[selected_instance_idx];
        let direction = Math.round(Math.random()* 4);
        let dest_direction;
        switch(direction)
        {
            case 0: dest_direction='top'; break;
            case 1: dest_direction='right'; break;
            case 2: dest_direction='bottom'; break;
            case 3: dest_direction='left'; break;
        }
        if(dest_direction)
        {
            moveSquare.call(this,dest_direction, true);
        }
    }

    ux.load_game = function(params)
    {
        console.log('load game');
        window.clearInterval(this.menu_timer);
        this.clear();
        this.reset_game();

        document.querySelector('.level-value').innerHTML=this.current_level;

        this.logic_params = params;
        this.num_moves=0;
        this.best_moves = params.best_moves;
        this.updateMovesValue();
        let square_width = 10;
        let init_x = (params.map.width/2)*square_width;
        let init_y = (params.map.height/2)*square_width;

        this.add({ "id":"scene", "type":"scene" });
        let max_width_height = Math.max(params.map.width, params.map.height);
        let center_of_scene = [ (params.map.width*square_width)/2, (params.map.height*square_width)/2, 0];

        // Camera and ambient lights
        let camera_pos = center_of_scene;
        if(params.camera_position)
        {
            camera_pos=params.camera_position;
        }
        this.add({
            "parent":"scene",
            "id": "camera_focus",
            "type":"object",
            "rotation":[0,0,0],
            "position": camera_pos
        });
        this.animate('camera_move', () =>
        {
            let val  = Math.sin(new Date().getTime()*0.00010)*0.1;
            this.ids.camera_focus.rotation.z=val;
            return true;
        });
        let camera_look = [0,0,0];
        if(params.camera_look)
        {
            camera_look = params.camera_look;
        }
        this.add({
            "parent":"camera_focus",
            "id": "camera",
            "type":"camera",
            "position": [0, -(params.map.height+2)*square_width, 100],
            "lookat":camera_look
        });
        this.add({ "parent":"scene",
            "id": "ambientlight",
            "type":"pointlight",
            "shadow": true,
            "position": [center_of_scene[0], center_of_scene[1], 100],
            "color": "#ffffff",
            "intensity": 0.4,
            "distance": 500 
        });
        this.add({ "parent":"scene",
            "id": "ambientlight",
            "type":"pointlight",
            "shadow": false,
            "position": [center_of_scene[0], (params.map.height+2)*square_width, 100],
            "color": "#ffffff",
            "intensity": 0.4,
            "distance": 500 
        });

        // Actual floor and grid
        this.add({
            "parent":"scene",
            "id":"background1",
            "type":"plane",
            "width":10000,
            "height":10000,
            "position": [ init_x, init_y, -1],
            "material": { "color":  main_bg_color}
        });
        this.add({
            "parent":"scene",
            "id":"background2",
            "type":"plane",
            "position": [ init_x, init_y, -0.1],
            "width":params.map.width*square_width + 2*square_width,
            "height":params.map.height*square_width + 2*square_width,
            "material": { "color":  default_color }
        });
        this.add({
            "parent":"scene",
            "id":"floorgrid",
            "type":"grid",
            "position": [ init_x, init_y, 0.10],
            "divisions_x": params.map.width+2,
            "divisions_y": params.map.height+2,
            "width":params.map.width*square_width + 2*square_width,
            "height":params.map.height*square_width + 2*square_width,
            "color": grid_color
        });

        this.add({ "parent":"scene",
            "id":"floor",
            "type":"plane",
            "position": [ init_x, init_y, 0],
            "width":params.map.width*square_width,
            "height":params.map.height*square_width,
            "material": { "color":  floor_color }
        });

        // Avoid clicking on the cube too fast
        this.action_timer = window.setTimeout(doAction.bind(this), 500);

        // Add the squares the player will move
        let image_x = 0;
        let image_y = 0;
        let destination_x =  params.map.destination.x;
        let destination_y =  params.map.destination.y;

        let image_step = 1/(params.image_items);

        let default_selected_idx = null;
        let playable_idx = 0;
        for(let i=0; i<params.image_items*params.image_items; i++)
        {
            let pos_data = params.map.items_data[i];
            if(pos_data.playable!==false)
            {
                if(default_selected_idx===null || pos_data.selected)
                {
                    default_selected_idx=playable_idx;
                }
                playable_idx++;
            }
            let square_params = JSON.parse(JSON.stringify(default_params));
            used_pos[posToString({x:pos_data.x,y: pos_data.y})]=1;
            square_params.materials.z = {
                metalness: 1,
                roughness: 0.5,
                color:'',
                map:params.image,
                scale: image_step,
                position: {
                    x:image_x,
                    y:image_y
                }
            };
            square_params.x = pos_data.x;
            square_params.y = pos_data.y;

            let square_instance = new square();
            square_instance.init(square_params);
            console.log('test',pos_data);
            if(pos_data.playable!==false)
            {
                console.log('add instance!');
                instances.push(square_instance);
                this.add_clickable(square_instance);
            }
            square_instance.correct_x = destination_x;
            square_instance.correct_y = destination_y;

            image_x += image_step;
            destination_x++;

            let rotation = params.map.destination.rotation;
            square_instance.correct_coords = 
                {
                    x: ['x', 1],
                    y: ['y', 1],
                    z: ['z', 1],
                }
            if(rotation==90)
            {
                square_instance.correct_coords.x = ['y',-1];
                square_instance.correct_coords.y = ['x',1];
            }
            if(rotation==180)
            {
                square_instance.correct_coords.x = ['x',-1];
                square_instance.correct_coords.y = ['y',-1];
            }
            if(rotation==-90)
            {
                square_instance.correct_coords.x = ['y',1];
                square_instance.correct_coords.y = ['x',-1];
            }

            if(image_x >= 1)
            {
                image_x = 0;
                image_y += image_step;
                destination_y++;
                destination_x =  params.map.destination.x;
            }
        }
        console.log('end instancse',instances, default_selected_idx);
        instances.forEach((instance) => {
            selected_instance = instance;
            checkAfterMove.call(this);
        });

        init_x = (params.image_items/2)*square_width;
        init_y = (params.image_items/2)*square_width;

        // Add invalid positions: squares with black color?
        if(params.map.invalid_positions)
        {
            params.map.invalid_positions.forEach((position) => {
                used_pos[posToString(position)]=1;
                let square_invalid_pos = new square();
                let square_invalid_params  = JSON.parse(JSON.stringify(default_params));
                square_invalid_params.x=position.x;
                square_invalid_params.y=position.y;
                square_invalid_params.z=0;
                square_invalid_params.size = square_width;
                square_invalid_params.parent='scene';
                square_invalid_params.id='obstacle';
                square_invalid_params.type='square';
                square_invalid_params.materials.z  = {color: default_color};
                square_invalid_pos.init(square_invalid_params);
            });
        }

        // Add the destination big square
        let destination  = {};
        let square_instance = new square();
        destination.parent='scene';
        destination.id='destination';
        destination.type='plane';
        destination.material  = {metalness: 1, roughness: 0.5, color:'#ffffff', opacity:1.0, map: params.image, rotation: params.map.destination.rotation };

        destination.position = [init_x+params.map.destination.x*square_width, init_y+params.map.destination.y*square_width, 0.2];
        destination.x = params.map.destination.x;
        destination.y = params.map.destination.y;
        destination.z = square_width;
        destination.width = params.image_items*square_width;
        destination.height = params.image_items*square_width;
        this.add(destination);
        //this.add(destination);


        selected_instance_idx=0;
        selected_instance = instances[selected_instance_idx];
        this.playable=params.playable;
        if(params.playable)
        {
            document.querySelector('.ui-playing').classList.add('visible');
            ux.bind('tap', this.auto_select_clicked.bind(this, manualSelection.bind(this)));
            //ux.bind('mousemove', this.auto_select_move.bind(this, manualSelection.bind(this)));
            ux.bind('swiperight', moveSquare.bind(this,'right'), false);
            ux.bind('swipeleft', moveSquare.bind(this,'left'), false);
            ux.bind('swipeup', moveSquare.bind(this,'top'), false);
            ux.bind('swipedown', moveSquare.bind(this,'bottom'), false);
            ux.bind('a', moveSquare.bind(this,'left'), false);
            ux.bind('d', moveSquare.bind(this,'right'), false);
            ux.bind('w', moveSquare.bind(this,'top'), false);
            ux.bind('s', moveSquare.bind(this,'bottom'), false);
            ux.bind('p', prevSelection.bind(this,true));
            ux.bind('n', nextSelection.bind(this,true));
            ux.bind('Escape', this.toggle_menu.bind(this));
            selected_instance_idx = default_selected_idx;
            selected_instance = instances[selected_instance_idx];
            selected_instance.enable_selected();
        }
        else
        {
            this.menu_timer =window.setInterval(this.random_moves.bind(this), 70);
        }
    };
})(this);

