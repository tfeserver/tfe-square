// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"3",
        "best_moves": 6,
        "map":
        {
            "width": 9,
            "height": 4,
            // where the items are initially
            "items_data":
            [
                { "x": 3, "y": 0, "playable": false },
                { "x": 4, "y": 0, "playable": false },
                { "x": 5, "y": 0, "playable": false },
                { "x": 3, "y": 1, "playable": false },
                { "x": 4, "y": 1, "playable": false },
                { "x": 5, "y": 1, "playable": false },
                { "x": 3, "y": 2, "playable": false },
                { "x": 0, "y": 2, "selected":true },
                { "x": 5, "y": 2, "playable": false }
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 3, "y": 0  }
        }
    });
})(this);


