// we are in ux environnement here
(function(ux)
{
    ux.load_game(
    {
        "playable": true,
        "image":"levels/puzzle/images/tfe.jpg",
        "image_items":"2",
        "best_moves": 6,
        "map":
        {
            "width": 6,
            "height": 6,
            // where the items are initially
            "items_data":
            [
                { "x": 2, "y": 2, "playable": false },
                { "x": 3, "y": 2, "playable": false },
                { "x": 2, "y": 3, "playable": false },
                { "x": 4, "y": 4, "selected":true },
            ],
            // Where the puzzle must be solved
            "destination": { "x" : 2, "y": 2, "rotation":0  }
        }
    });
})(this);


