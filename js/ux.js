"use strict";

class Ux
{

    constructor(params)
    {
        if(!params || !params.root)
        {
            console.log('error ',this, params);
            throw new Error('No root parameter received.');
        }

        this.root = params.root;
        this.current_level = 1;
        let re;
        // DEBUG
        if(re = location.href.match(/level=(\d+)/))
        {
            this.current_level=re[1];
        }
        this.ids = {};
        this.loops = {};
        this.animations = {};
        this.clock = new THREE.Clock();

        // Key bindings
        this.pressed = [];
        this.binds = {};
        this.auto_unbind_events= [ 'mousemove', 'tap','swipeleft','swiperight','swipeup','swipedown' ];
    }

    log(item)
    {
        console.log('LOG: ',item);
    }

    build()
    {
        window.addEventListener('resize', this.resize.bind(this));

        this.init();
    }

    socket_error(item)
    {
        alert(item.message);
    }

    clear()
    {
        for(let id in this.ids)
        {
            if(this.ids[id].type=='html')
            {
                //this.ids[id].dom.remove();
            }
        }
        if(this.ids.scene)
        {
            let scene = this.ids.scene;
            while(scene.children.length > 0){ 
                scene.remove(scene.children[0]); 
            }
        }
        this.ids = {};
        this.loops = {};
        this.animations = {};
        this.clock = new THREE.Clock();
        this.binds = {};
    }

    register_item(item, added)
    {
        if(item.id)
        {
            if(!this.ids[item.id])
            {
                this.ids[item.id] = added;
            }
            else
            {
                throw new Error('Cannot create same id '+item.id);
            }
        }
        return added;
    }

    animate(id, fct)
    {
        this.animations[id] =  fct;
    }

    applyParams(container, item)
    {
        container.position.x = item.position[0];
        container.position.y = item.position[1];
        container.position.z = item.position[2];
        if(item.rotation)
        {
            container.rotation.x = degToRad(item.rotation[0]);
            container.rotation.y = degToRad(item.rotation[1]);
            container.rotation.z = degToRad(item.rotation[2]);
        }
        if(item.scale)
        {
            container.scale.x = item.scale[0];
            container.scale.y = item.scale[1];
            container.scale.z = item.scale[2];
        }
        if(item.lookat)
        {
            container.lookAt(item.lookat[0], item.lookat[1], item.lookat[2]);
        }
        if(item.parent)
        {
            this.ids[item.parent].add(container);
        }
        else
        {
            this.scene.add(container);
        }
    }

    status(item)
    {
        console.log('status ',item);
        //document.querySelector('.players .status-value').innerHTML=item.players;
    }

    loadjs(item)
    {
        let fct = new Function(item.data);
        fct.call(this);
    }

    create_texture(item)
    {
        item = Object.assign({}, item);
        if(item.map)
        {
            var texture = new THREE.TextureLoader().load(item.map);
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            if(item.scale)
            {
                texture.repeat.set(item.scale, item.scale);
            }
            if(item.position)
            {
                texture.offset.x = item.position.x;
                texture.offset.y = item.position.y;
            }
            if(item.rotation) {
                texture.rotation = degToRad(item.rotation);
            }
            var obj =  { map: texture,  ambient: 0x030303, specular:0x666666, shininess: 30 };
            if(item.color) {
                obj.color = item.color;
            }
            if(typeof item.opacity!='undefined') {
                obj.transparent = true;
                obj.opacity = item.opacity;
            }
            return new THREE.MeshBasicMaterial(obj);
        }
        var obj = { emissive: '#ffffff'  };
        ['color','roughness', 'specular', 'flatShading','wireframe', 'emissive', 'ambient','specular','shininess'].forEach((key) => {
            if(typeof item[key]!='undefined')
            {
                obj[key] = item[key];
            }
        });
        if(typeof item.opacity!='undefined') {
            obj.transparent = true;
            obj.opacity = item.opacity;
        }
        return new THREE.MeshBasicMaterial(obj);
    }

    add(item)
    {
        let geometry, material, mesh;
        let self = this;

        switch(item.type)
        {
            case 'scene':
                this.scene = new THREE.Scene();
                return this.register_item(item,this.scene);
                break;

            case 'camera':
                this.camera = new THREE.PerspectiveCamera( 50, this.root.clientWidth / this.root.clientHeight, 1, 1000 );
                this.applyParams(this.camera, item);

                return this.register_item(item, this.camera);
                break;

            case 'plane':
                geometry = new THREE.PlaneGeometry( item.width, item.height, 1 );
                material = this.create_texture(item.material);
                mesh = new THREE.Mesh( geometry, material );
                mesh.receiveShadow = true;
                mesh.castShadow = true;
                this.applyParams(mesh, item);
                return this.register_item(item, mesh);
                break;

            case 'axis':
                mesh = new THREE.AxesHelper(15);
                this.applyParams(mesh, item);
                return mesh;


            case 'grid':
                var step_x = item.width / (item.divisions_x);
                var step_y = item.height / (item.divisions_y);
                var init_x = -item.width/2;
                var init_y = -item.height/2;
                var x = 0;
                var y =0;
                // Params
                var object = new THREE.Object3D();
                for(var i=0; i<=item.divisions_x; i++)
                {
                    var material2 = new THREE.LineBasicMaterial( {color: item.color} );
                    var geometry2 = new THREE.Geometry();
                    geometry2.vertices.push(new THREE.Vector3(init_x+i*step_x, init_y, item.position.z) );
                    geometry2.vertices.push(new THREE.Vector3(init_x+i*step_x, init_y+item.height, item.position.z) );
                    var line = new THREE.Line( geometry2, material2 );
                    object.add(line);
                }
                for(var i=0; i<=item.divisions_y; i++)
                {
                    material2 = new THREE.LineBasicMaterial( {color: item.color} );
                    geometry2 = new THREE.Geometry();
                    geometry2.vertices.push(new THREE.Vector3(init_x, init_y+i*step_y, item.position.z) );
                    geometry2.vertices.push(new THREE.Vector3(init_x+item.width, init_y+i*step_y, item.position.z) );
                    var line = new THREE.Line( geometry2, material2 );
                    object.add(line);
                }
                this.applyParams(object, item);
                return this.register_item(item, object);
                break;

            case 'pointlight':
                var light = new THREE.PointLight( item.color, item.intensity, item.distance );
                light.shadow.mapSize.width = 512;  // default
                light.shadow.mapSize.height = 512; // default
                light.shadow.camera.near = 0.5;       // default
                light.shadow.camera.far = 5000      // default
                light.castShadow = item.shadow;
                if(item.shadow)
                {
                    item.shadowMapWidth=1024;
                    item.shadowMapheight=1024;
                }
                this.applyParams(light, item);
                return light;
                break;

            case 'html':
                var div =document.createElement('div');
                if(item.class) { div.setAttribute('class', item.class); }
                div.innerHTML = item.html;
                item.dom = div;
                this.root.append(div);
                this.register_item(item, item);
                break;

            case 'cube':
                var materials = [];
                if(item.materials)
                {
                    materials.push(this.create_texture(item.materials['x']));
                    materials.push(this.create_texture(item.materials['-x']));
                    materials.push(this.create_texture(item.materials['y']));
                    materials.push(this.create_texture(item.materials['-y']));
                    materials.push(this.create_texture(item.materials['z']));
                    materials.push(this.create_texture(item.materials['-z']));
                }
                else
                {
                    materials = this.create_texture({ metalness:1 , roughness: 0.5, color: item.color});
                }
                if(item.width)
                {
                    geometry = new THREE.BoxGeometry( item.width, item.height, item.depth, 1, 1, 1);
                }
                else
                {
                    geometry = new THREE.CubeGeometry( item.size, item.size, item.size, 1, 1, 1);
                }


                mesh = new THREE.Mesh( geometry, materials);
                mesh.receiveShadow = true;
                mesh.castShadow = true;
                this.applyParams(mesh, item);

                this.register_item(item, mesh);
                return mesh;
                break;

            case 'object':
                var object = new THREE.Object3D();
                this.register_item(item, object);
                this.applyParams(object, item);
                return object;
                break;

            case 'gltf':
                var loader = new THREE.GLTFLoader();
                var obj = new THREE.Object3D();
                loader.load(item.href, function ( gltf ) {
                    obj.scale.x=1.0;
                    obj.scale.y=1.0;
                    obj.scale.z=1.0;

                    self.applyParams(obj, item);
                    self.register_item(item, obj);

                    var material;
                    if(item.material)
                    {
                        material = self.create_texture(item.material);
                    }

                    gltf.scene.traverse( function ( child ) {
                        obj.add(child);
                        child.material = material;
                    } );
                    if (gltf.animations && gltf.animations.length) {
                        self.mixer = new THREE.AnimationMixer(gltf.scene);
                        for (var i = 0; i < gltf.animations.length; i++) {
                            var animation = gltf.animations[i];
                            //self.mixer.clipAction(animation).play();
                        }
                    }
                } );
                return obj;
                break;

            default:
                console.error('Do not know what item to add ',item);
        }
    }

    send(item)
    {
        this.client.send(item.data);
    }


    init()
    {
        this.renderer = new THREE.WebGLRenderer({ antialias: true});
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

        this.renderer.setSize(this.root.clientWidth, this.root.clientHeight);
        this.root.appendChild(this.renderer.domElement );
        this.hammertime = new Hammer(this.root, {});
        this.hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL});
        this.hammer_last_event = null;
        this.raycaster = new THREE.Raycaster();

        this.auto_unbind_events.forEach((name) =>
        {
            this.hammertime.on(name, (evt) =>
            {
                this.hammer_last_event= evt;
                if(this.pressed.indexOf(name)===-1)
                {
                    this.pressed.push(name);
                }
            });
        });

        this.client.send({ command: 'connected', game:'puzzle'});

        document.addEventListener('keydown', this.keydown.bind(this));
        document.addEventListener('keyup', this.keyup.bind(this));
        document.addEventListener('mousemove', this.mousemove.bind(this));

    }

    mousemove(e)
    {
        this.mouse_event=e;
        if(this.pressed.indexOf('mousemove')===-1)
        {
            this.pressed.push('mousemove');
        }
    }

    keydown(e)
    {
        if(this.pressed.indexOf(e.key)===-1)
        {
            this.pressed.push(e.key);
        }
    }
    keyup(e)
    {
        this.pressed = this.pressed.filter(x => x !== e.key);
    }

    resize()
    {
        if(this.camera)
        {
            this.camera.aspect = this.root.clientWidth / this.root.clientHeight
            this.camera.updateProjectionMatrix()
        }
        if(this.renderer)
        {
            this.renderer.setSize(this.root.clientWidth, this.root.clientHeight)
        }
    }

    loop()
    {
        this.delta = this.clock.getDelta();
        // Handle user interaction
        this.pressed.forEach(pressed => 
        {
            if(this.binds[pressed])
            {
                this.binds[pressed]();
                if(this.auto_unbind_events.indexOf(pressed)!==-1)
                {
                    this.pressed = this.pressed.filter(x => x !== pressed);
                }
            }
        });

        for(var idx in this.animations)
        {
            if(!this.animations[idx](this.delta))
            {
                delete this.animations[idx];
            }
        }
        if(this.mixer)
        {
            this.mixer.update(delta);
        }

        //this.cube.rotation.z += 0.05;
        if(this.scene && this.camera)
        {
            this.renderer.render(this.scene, this.camera);
        }
        requestAnimationFrame(this.loop.bind(this));
    }

    error(msg)
    {
        console.error('An error occured.',msg);
    }

    unbindall()
    {
        this.binds = [];
    }
    unbind(bind_key)
    {
        this.binds = this.pressed.filter(x => x !== bind_key);
    }

    bind(key, fct)
    {
        this.binds[key]= fct;
    }
    reset_clickables()
    {
        this.clickables = [];
    }
    add_clickable(item)
    {
        item.boundbox.parent_instance = item;
        this.clickables.push(item.boundbox);
    }

    auto_select_clicked(callback,evt)
    {
        console.log('auto select clicked');
        let mouse = { 
            x: 2 * (this.hammer_last_event.center.x / this.root.offsetWidth) - 1,
            y: - (this.hammer_last_event.center.y / this.root.offsetHeight) * 2 + 1
        };

        this.raycaster.setFromCamera(mouse, this.ids.camera );
        let intersects = this.raycaster.intersectObjects(this.clickables);
        intersects.forEach((item) =>
        {
            return callback(item.object.parent_instance);
        });
    }

    auto_select_move(callback,evt)
    {
        console.log('auto select clicked');
        let mouse = { 
            x: 2 * (this.mouse_event.clientX / this.root.offsetWidth) - 1,
            y: - (this.mouse_event.clientY / this.root.offsetHeight) * 2 + 1
        };
        console.log('test',mouse);

        this.raycaster.setFromCamera(mouse, this.ids.camera );
        let intersects = this.raycaster.intersectObjects(this.clickables);
        intersects.forEach((item) =>
        {
            return callback(item.object.parent_instance);
        });
    }
}

