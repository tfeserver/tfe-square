"use strict";

class Server
{
    construct()
    {
        this.params= {};
    }

    async connect(_params){
        let self=this;

        this.params = _params;
        return new Promise(function(ok, reject)
        {
            try
            {
                let exampleSocket = new WebSocket("ws://"+self.params.host+"/",'tfe-game_v0');
                exampleSocket.onerror = (event) =>
                {
                    self.handleMessage({ event: 'socket_error', message:'Error Connecting to server' });
                    ok(false);
                };
                exampleSocket.onopen = (event) =>
                {
                    ok(true);
                };

                exampleSocket.onmessage = (event) =>
                {
                    try
                    {
                        let msg = JSON.parse(event.data);
                        self.handleMessage(JSON.parse(event.data));
                    }
                    catch(err)
                    {
                    }
                };

                exampleSocket.onclose = (event) =>
                {
                    self.handleMessage({ command: 'close', message: 'Socket closed' });
                };
            }
            catch(err)
            {
                console.log('connect catch error fail',err);
                reject('fail');
            }
        });
    }
    async handleMessage(msg)
    {
        if(Array.isArray(msg))
        {
            msg.forEach(submsg => this.handleMessage(submsg));
            return  true;
        }

        if(this.params.ux[msg.command])
        {
            this.params.ux[msg.command](msg);
        }
        else
        {
            console.error('Do not know what to do: ',msg);
        }
    }

}

