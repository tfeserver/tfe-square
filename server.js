#!/usr/bin/env node


var protocol = 'tfe-game_v0';
var fs = require('fs'), path = require('path');

var ssl_enabled = true;
process.argv.forEach(function(val, index, array)
{
    if(val=='-nossl')
    {
        ssl_enabled=false;
    }
});

const http = ssl_enabled ? require('https') : require('http');

const server = ssl_enabled ?
    new http.createServer({
        cert: fs.readFileSync('/etc/letsencrypt/live/tfeserver.fr/fullchain.pem'),
        key: fs.readFileSync('/etc/letsencrypt/live/tfeserver.fr/privkey.pem')
    }) : 
    new http.createServer({ });
    ;
server.listen(6060, function() {});

var WebSocketServer = require('websocket').server;
 
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
    console.log('received',origin);
  return true;
}
 
var last_id = 1;
var connections= [];



wsServer.on('request', function(request) {
	console.log('received',request);
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    // Only accept our great protocol
    if (request.requestedProtocols.indexOf(protocol) === -1) {
        return request.reject();
    }
    
    var connection = request.accept(protocol, request.origin);
    var connection_id = ++last_id;


    connections['con'+last_id] = connection;



    connection.on('message', function(message) {
        console.log('received ',message);
        if (message.type === 'utf8') {
            try
            {
                var msg = JSON.parse(message.utf8Data);
                if(msg.command == 'connected')
                {
                    var game = msg.game;
                    game = game.replace(/[^a-zA-Z0-9]/,"").substr(0, 20);
                    console.log('sending via connected');
                    var filePath = path.join(__dirname,'levels/'+game+'/load.js');
                    var GameServer = require('./levels/'+game+'/server.js');
                    connection.server = new GameServer(connections, connection);
                    fs.readFile(filePath, function(err, data)
                    {
                        commands = JSON.parse(data.toString());
                        commands.forEach(function(command)
                        {
                            if(command.command=='loadjs')
                            {
                                var filePath = command.file;
                                var data  = fs.readFileSync(filePath);
                                command.data = data.toString();
                                connection.send(JSON.stringify(command));
                            }
                            else if(typeof command!='string')
                            {
                                connection.send(JSON.stringify(command));
                            }
                        });
                    });
                }
                if(connection.server[msg.command])
                {
                    connection.server[msg.command](msg);
                }
                else
                {
                }
            }
            catch(err)
            {
                console.log('cannot parse '+message.utf8Data, msg.command, err);
            }
            //connection.sendUTF(JSON.stringify({ action:'ok', message: message.utf8Data}));
        }
    });
    connection.on('close', function(reasonCode, description) {
        delete connections['con'+connection_id];
        connection.server.disconnected();
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
